module gitlab.com/kendellfab/web

go 1.13

require (
	github.com/google/uuid v1.1.2 // indirect
	github.com/minio/minio-go/v7 v7.0.5
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	golang.org/x/crypto v0.0.0-20201116153603-4be66e5b6582 // indirect
	golang.org/x/net v0.0.0-20201110031124-69a78807bb2b // indirect
	golang.org/x/sys v0.0.0-20201116194326-cc9327a14d48 // indirect
	golang.org/x/text v0.3.4 // indirect
	gopkg.in/ini.v1 v1.62.0 // indirect
)
