package messaging

import "fmt"

type Email interface {
	GetName() string
	GetAddress() string
}

type EmailImpl struct {
	Name    string `json:"name"`
	Address string `json:"address"`
}

func (e EmailImpl) GetName() string {
	return e.Name
}

func (e EmailImpl) GetAddress() string {
	return e.Address
}

func (e EmailImpl) String() string {
	return fmt.Sprintf("Name: %s Address %s", e.Name, e.Address)
}
