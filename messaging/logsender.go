package messaging

import (
	"fmt"
	"log"
)

type LogSender struct {
}

func (l LogSender) Send(to, from Email, message Message) error {
	log.Println(fmt.Sprintf("To: `%s` <%s>", to.GetName(), to.GetAddress()))
	log.Println(fmt.Sprintf("From: '%s' <%s>", from.GetName(), from.GetAddress()))
	log.Println(message)
	return nil
}
