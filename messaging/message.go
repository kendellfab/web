package messaging

import "fmt"

type Message interface {
	GetSubject() string
	GetPlain() string
	GetHtml() string
}

type MessageImpl struct {
	Subject string `json:"subject"`
	Plain   string `json:"plain"`
	Html    string `json:"html"`
}

func (m MessageImpl) GetSubject() string {
	return m.Subject
}

func (m MessageImpl) GetPlain() string {
	return m.Plain
}

func (m MessageImpl) GetHtml() string {
	return m.Html
}

func (m MessageImpl) String() string {
	return fmt.Sprintf("Plain: %s \n\n Html: %s", m.Plain, m.Html)
}
