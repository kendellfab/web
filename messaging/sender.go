package messaging

type Sender interface {
	Send(to, from Email, message Message) error
}
