package upload

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"path/filepath"
	"strconv"
	"time"
)

// FileUpload provides a set of methods to be registered as Post, Get for uploading/getting files
type FileUpload struct {
	url             string
	driver          StorageDriver
	downloadAllowed bool
}

// NewFileUpload creates a new fileupload struct
func NewFileUpload(url string, driver StorageDriver, opts ...func(*FileUpload) error) FileUpload {
	f := FileUpload{url: url, driver: driver}
	for _, opt := range opts {
		opt(&f)
	}
	return f
}

// DoUpload is expecting a post request.
// A form file named "file" is the expected form file name
// Returns a result via json, upon successful upload
func (f FileUpload) DoUpload(w http.ResponseWriter, r *http.Request) {
	file, header, err := r.FormFile("file")
	if err != nil {
		log.Println("error uploading file:", err)
		http.Error(w, "error uploading file", http.StatusInternalServerError)
		return
	}
	defer file.Close()

	yr, m, _ := time.Now().Date()
	key := fmt.Sprintf("%d/%d/%s", yr, m, header.Filename)
	buf := bufio.NewReader(file)
	contentType, err := detectContentType(buf)
	if err != nil {
		log.Println("error sniffing data:", err)
		http.Error(w, "error sniffing data type", http.StatusInternalServerError)
		return
	}

	storageErr := f.driver.PutObject(r.Context(), key, buf, header.Size, contentType)
	if storageErr != nil {
		log.Println("error storing file", storageErr)
		http.Error(w, "error storing file", http.StatusInternalServerError)
		return
	}

	res := Result{Status: "success", Key: key, Path: f.url + "/" + key}
	json.NewEncoder(w).Encode(res)
}

// HandleGet returns the file from the driver.
// download=true query parameter to initiate download, if allowed on the struct
func (f FileUpload) HandleGet(w http.ResponseWriter, r *http.Request) {
	key := extractKey(r, f.url)
	obj, err := f.driver.GetObject(r.Context(), key)
	if err != nil {
		log.Println("error loading file", err)
		http.Error(w, "error loading file", http.StatusInternalServerError)
		return
	}

	download, _ := strconv.ParseBool(r.URL.Query().Get("download"))
	buf := bufio.NewReader(obj)
	if download && f.downloadAllowed {
		contentType, err := detectContentType(buf)
		if err != nil {
			log.Println("error detecting content type for download", err)
		} else {
			w.Header().Add("Content-Type", contentType)
			w.Header().Add("Content-Disposition", fmt.Sprintf("attachment; filename=\"%s\"", filepath.Base(key)))
		}
	}

	_, copyErr := io.Copy(w, buf)
	if copyErr != nil {
		log.Println("error copying file", copyErr)
	}
}

func AllowDownload(allowDownload bool) func(*FileUpload) error {
	return func(f *FileUpload) error {
		f.downloadAllowed = true
		return nil
	}
}

func detectContentType(buf *bufio.Reader) (string, error) {
	bs, err := buf.Peek(512)
	if err != nil {
		return "", err
	}

	contentType := http.DetectContentType(bs)
	return contentType, nil
}
