package upload

import (
	"bytes"
	"encoding/json"
	"fmt"
	"image"
	_ "image/gif"
	"image/jpeg"
	_ "image/png"
	"io"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/nfnt/resize"
)

type ImageUploadConfig func(upload *ImageUpload)

func ImageUploadResize(width uint) ImageUploadConfig {
	return func(iu *ImageUpload) {
		iu.doResize = true
		iu.width = width
	}
}

// ImageUpload provides a set of methods to be registered as Post, Get, Delete for uploading images.
type ImageUpload struct {
	url      string
	driver   StorageDriver
	doResize bool
	width    uint
}

// NewImageUpload creates a new imageupload struct.
// url: this is the url that the resultant path with be prepended with for getting an image
// driver: is the storage driver for storing the image
// opts: a list of optional config components
func NewImageUpload(url string, driver StorageDriver, opts ...ImageUploadConfig) ImageUpload {
	i := ImageUpload{url: url, driver: driver}
	for _, opt := range opts {
		opt(&i)
	}
	return i
}

// DoUpload is expecting a post request.
// A form file named "file" is the expected form file name
// Returns a result via json response, upon successful upload
func (i ImageUpload) DoUpload(w http.ResponseWriter, r *http.Request) {
	file, header, err := r.FormFile("file")
	if err != nil {
		log.Println("error uploading file:", err)
		http.Error(w, "error uploading file", http.StatusInternalServerError)
		return
	}
	defer file.Close()

	img, _, imgErr := image.Decode(file)
	if imgErr != nil {
		log.Println("error decoding image:", imgErr)
		http.Error(w, "error decoding image", http.StatusInternalServerError)
		return
	}

	if i.doResize {
		img = resize.Resize(i.width, 0, img, resize.Lanczos3)
	}

	var buf bytes.Buffer
	encErr := jpeg.Encode(&buf, img, nil)
	if encErr != nil {
		log.Println("error encoding image:", encErr)
		http.Error(w, "error encoding image", http.StatusInternalServerError)
		return
	}

	yr, m, _ := time.Now().Date()
	key := fmt.Sprintf("%d/%d/%s", yr, m, header.Filename)

	storageErr := i.driver.PutObject(r.Context(), key, &buf, int64(buf.Len()), "image/jpeg")
	if storageErr != nil {
		log.Println("error storing image", storageErr)
		http.Error(w, "error storing image", http.StatusInternalServerError)
		return
	}

	res := Result{Status: "success", Key: key, Path: i.url + "/" + key}

	json.NewEncoder(w).Encode(res)
}

// HandleGet will load the image.
// Expects to be registered under a url that looks like: i.url + /{year}/{month}/{file}
func (i ImageUpload) HandleGet(w http.ResponseWriter, r *http.Request) {
	key := extractKey(r, i.url)
	obj, err := i.driver.GetObject(r.Context(), key)
	if err != nil {
		log.Println("error loading image", err)
		http.Error(w, "error loading image", http.StatusInternalServerError)
		return
	}
	_, copyErr := io.Copy(w, obj)
	if copyErr != nil {
		log.Println("error copying image", copyErr)
	}
}

func extractKey(r *http.Request, url string) string {
	return strings.Replace(r.URL.Path, url+"/", "", -1)
}
