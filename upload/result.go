package upload

// Result is the response of an upload.  With the key and path.
// The path is the key prepended with the url for the image uploader.
type Result struct {
	Status string `json:"status"`
	Key    string `json:"key"`
	Path   string `json:"path"`
}
