package upload

import (
	"context"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/minio/minio-go/v7"
)

// StorageDriver is an interface for storing bytes
type StorageDriver interface {
	PutObject(ctx context.Context, key string, r io.Reader, objectSize int64, contentType string) error
	GetObject(ctx context.Context, key string) (io.ReadCloser, error)
	DeleteObject(ctx context.Context, key string) error
}

// FileStorage represents a StorageDriver that uses the local filesystem.
type FileStorage struct {
	baseDir string
}

// NewFileStorage creates a new FileStorage with the given baseDir as the root.
func NewFileStorage(baseDir string) StorageDriver {
	err := os.MkdirAll(baseDir, 0755)
	if err != nil {
		log.Fatal("Error creating storage directory", err)
	}

	return &FileStorage{baseDir: baseDir}
}

func (f *FileStorage) PutObject(ctx context.Context, key string, r io.Reader, objectSize int64, contentType string) error {

	base := filepath.Base(key)
	err := os.MkdirAll(filepath.Join(f.baseDir, strings.Replace(key, base, "", -1)), 0755)
	if err != nil {
		return err
	}

	out, err := os.Create(filepath.Join(f.baseDir, key))
	if err != nil {
		return err
	}
	defer out.Close()
	_, err = io.Copy(out, r)
	return err
}

func (f *FileStorage) GetObject(ctx context.Context, key string) (io.ReadCloser, error) {
	in, err := os.Open(filepath.Join(f.baseDir, key))
	if err != nil {
		return nil, err
	}

	return in, nil
}

func (f *FileStorage) DeleteObject(ctx context.Context, key string) error {
	path := filepath.Join(f.baseDir, key)
	return os.Remove(path)
}

// S3Storage represents a StorageDriver that uses S3 or acceptable alternatives
type S3Storage struct {
	bucket string
	mc     *minio.Client
}

// NewS3Storage creates a new StorageDriver with the given bucket and a minio client.  Creates the bucket if it doesn't exist.
func NewS3Storage(bucket string, mc *minio.Client) StorageDriver {
	ctx := context.Background()
	exists, err := mc.BucketExists(ctx, bucket)
	if err == nil && !exists {
		err = mc.MakeBucket(ctx, bucket, minio.MakeBucketOptions{})
		if err != nil {
			log.Fatal("Error creating bucket:", err)
		}
	}

	return &S3Storage{bucket: bucket, mc: mc}
}

func (s *S3Storage) PutObject(ctx context.Context, key string, r io.Reader, objectSize int64, contentType string) error {
	_, err := s.mc.PutObject(ctx, s.bucket, key, r, objectSize, minio.PutObjectOptions{ContentType: contentType})
	return err
}

func (s *S3Storage) GetObject(ctx context.Context, key string) (io.ReadCloser, error) {
	return s.mc.GetObject(ctx, s.bucket, key, minio.GetObjectOptions{})
}

func (s *S3Storage) DeleteObject(ctx context.Context, key string) error {
	return s.mc.RemoveObject(ctx, s.bucket, key, minio.RemoveObjectOptions{})
}
